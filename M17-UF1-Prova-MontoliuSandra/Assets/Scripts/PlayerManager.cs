using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private float _positionMultiplier = 0.005f;
    public bool Alive = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ControlPlayer();
        if (Input.GetKeyDown("space")) Shoot();
            
    }

    private void ControlPlayer()
    {
        transform.position = new Vector3(_positionMultiplier * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z); // Cambiar X
        
        transform.position = new Vector3(transform.position.x, _positionMultiplier * Input.GetAxis("Vertical") + transform.position.y, transform.position.z); // Cambiar Y

    }

    private void Shoot()
    {
        GameObject.Find("fireball-red-tail-big Variant (1)").transform.position = GameObject.Find("2DFighterCanon").transform.position;
        GameObject.Find("fireball-red-tail-big Variant").transform.position = GameObject.Find("2DFighterCanon (1)").transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        //GameObject.Find("Main Camera").GetComponent<GameManager>().Reiniciar();
        switch (collision.gameObject.name)
        {
            case "Alien - SpeedFlight":
                Alive = false;
                break;
        }
    }
}
