using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int Points = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameObject.Find("PlayerShip").GetComponent<PlayerManager>().Alive) Reiniciar();

        GameObject.Find("Points").GetComponent<Text>().text = GameObject.Find("Main Camera").GetComponent<GameManager>().Points + " Points";
    }

    public void Reiniciar()
    {
        Points = 0;

        GameObject.Find("PlayerShip").GetComponent<PlayerManager>().Alive = true;
        GameObject.Find("PlayerShip").transform.position = new Vector3(0, -1, 0);

        GameObject.Find("fireball-red-tail-big Variant").transform.position = new Vector3(0, 11, 0);
        GameObject.Find("fireball-red-tail-big Variant (1)").transform.position = new Vector3(0, 11, 0);
        GameObject.Find("fireball-red-tail-big Variant (2)").transform.position = new Vector3(0, -4, 0);

        GameObject.Find("Alien - SpeedFlight").transform.position = new Vector3(5, 9, 0);
        GameObject.Find("Alien - SpeedFlight").GetComponent<EnemicManager>().Alive = true;
    }
}
