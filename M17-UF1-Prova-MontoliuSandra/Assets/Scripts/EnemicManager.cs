using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemicType
{
    SpeedFlight,
    Fragate
}
public class EnemicManager : MonoBehaviour
{
    public EnemicType EnemicType;
    public bool Alive = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!Alive)
        {
            DestroyObject(this.gameObject);
            GameObject.Find("Main Camera").GetComponent<GameManager>().Points += 5;
        }
        else
        {
            switch (EnemicType)
            {
                case EnemicType.SpeedFlight:
                    transform.position = new Vector3(transform.position.x, transform.position.y - 0.005f, transform.position.z);
                    break;
                case EnemicType.Fragate:
                    transform.position = new Vector3(transform.position.x, transform.position.y - 0.0005f, transform.position.z);
                    break;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Alive = false;
    }
}
